import { Component, Element, Prop, Method } from '@stencil/core';
import { ILocalizationService } from '../../localization';

@Component({
    tag: 'locale-text'
})
export class LocaleText {
    @Element() el:HTMLElement;
    @Prop() id: string;
    @Prop() context?:any;
    @Prop() theme?:string = "locale-text"
    render() {
        return (
          <span class={this.theme}><slot></slot></span>
        )
    }
    @Method()
    visit(translator:ILocalizationService) {
        let ctx = null;
        if (this.context && typeof this.context === 'string') {            
            ctx = JSON.parse(this.context.replace(/'/g, '"'));            
        } else {
            ctx = this.context;
        }

        let translation = 
            ctx && translator.translateWithProps(this.id, ctx) 
            || translator.translate(this.id);
        
        if (!translation) {
            return;
        }
        this.el.innerText = translation; 
    }
    componentWillLoad () {
        
    }
}