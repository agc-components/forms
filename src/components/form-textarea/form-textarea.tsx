
import { Component, Element, Prop, State, Event, EventEmitter, Method } from '@stencil/core';
import { IValidationServiceInjector, IValidationService } from '../../validation';
import { ILocalizationService } from '../../localization';

@Component({
    tag: 'form-textarea',
    styleUrl: 'form-textarea.css'
})
export class FormTextarea {
    @Element() el:HTMLElement;
    @Prop({connect: 'validation-service-injector'}) injector: IValidationServiceInjector;
    @Prop() theme?: string = 'form-input';
    @Prop() name: string;
    @Prop({mutable: true}) value: string;
    @Prop() maxLength?:number;
    @Prop() lines?:number = 5;
    @Prop() required?: string;
    @Prop() pattern?: string;

    @State() remaining:number;
    @State() errorMessage:string;
    

    @Event() valueChange: EventEmitter;
    validator:IValidationService;
    localizer:ILocalizationService;

    @Method()
    visit(localizer:ILocalizationService) {
        this.localizer = localizer;
    }

    inputChanged(ev: any) {
        let val = ev.target && ev.target.value;        

        if (this.maxLength != undefined) {
            let charactersLeft = this.maxLength - val.length;
            this.remaining = charactersLeft > 0 ? charactersLeft : 0;

            if (this.remaining <= 0) {               
                val = val.slice(0, this.maxLength);
                const textarea = this.el.querySelector('textarea');
                textarea.value = val;
            }
        }        

        this.value = val;
        this.valueChange.emit(this.value);

        let error = this.validator.hasError(ev.target);
        if (error) {
            if (this.localizer) {
                this.errorMessage = this.localizer.translate(error);
                return error;
            }

            this.errorMessage = error;
            return error;
        }
        
        this.errorMessage = null;
    }

    render () {
        return (

            <div class={this.theme} data-input-type="textarea">
              <label><slot name="label"></slot></label>
              <textarea rows={this.lines} id={this.name} name={this.name} onInput={this.inputChanged.bind(this)}>{this.value}</textarea>              
              <p>                
                { this.errorMessage && <span class="error-message">{this.errorMessage}</span> }
                { this.maxLength && <span>{this.remaining} <locale-text id="characters_remaining">characters remaining</locale-text></span> }
              </p>
            </div>
          )
    }
    componentWillLoad () {
        this.injector.create().then(validationService => {
            this.validator = validationService;
        });
    }
    componentDidLoad () {
        if (this.maxLength) {
            this.remaining = this.maxLength;
        }
        const inputEl = this.el.querySelector('textarea');
        if (this.required) {
            inputEl.setAttribute('required', '');
        }
        if (this.pattern) {
            inputEl.setAttribute('pattern', this.pattern);
        }
    }
}