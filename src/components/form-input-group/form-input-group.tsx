import { Component, Element, Prop } from '@stencil/core';

@Component({
    tag: 'form-input-group',
    styleUrl: 'form-input-group.css'
})
export class FormInputGroup {
    @Element() el:HTMLElement;
    @Prop() theme?:string = 'form-input-group-wrapper';
    @Prop() flexible?:string = "true";

    render() {
        let cls = this.theme;
        if (!!this.flexible) {
            cls = cls + ' flexible';
        }
        return (
          <div class={cls}>
            <label><slot name="label"></slot></label>
            <div class='form-input-group'>
                <slot></slot>
            </div>
          </div>
        )
    }
    componentDidLoad () {
        const children = this.el.querySelectorAll('form-input, form-select');
        console.log('children', children);
    }
    
}