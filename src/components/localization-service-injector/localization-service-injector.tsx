import {Component, Method} from '@stencil/core';
import { jsonGet } from '../../util';
import {LocalizationService, ILocalizationServiceInjector, ILocalizationService} from "../../localization";

@Component({
    tag: 'localization-service-injector'
})
export class LocalizationServiceInjector implements ILocalizationServiceInjector {
    @Method()
    create(localeUrl?:string): Promise<ILocalizationService> {

        return new Promise(resolve => {
            if (localeUrl != undefined) {
                jsonGet(localeUrl, (messages) => {
                    resolve(new LocalizationService(messages));
                });
            } else {
                resolve(new LocalizationService({}));
            }
        });
    }
    
}