import { Component, Element, Event, EventEmitter, Prop, Method, Watch } from '@stencil/core';
import { IValidationServiceInjector, IValidationService } from '../../validation';
import { ILocalizationService } from '../../localization';

@Component({
    tag: 'form-select',
    styleUrl: 'form-select.css'
})
export class FormSelect {
    @Element() el:HTMLElement;
    @Prop({connect: 'validation-service-injector'}) injector: IValidationServiceInjector;
    @Prop() name: string;
    @Prop() theme?: string = 'form-input';
    @Prop({mutable: true}) value: string;
    @Prop() required?: string;
    @Event() valueChange: EventEmitter;
    validator:IValidationService;
    localizer:ILocalizationService;

    @Watch('value')
    valueChanged() {
        const selectEl = this.el.querySelector('select');
        if (selectEl.value !== this.value) {
            selectEl.value = this.value;
        }
    }

    @Method()
    visit(localizer:ILocalizationService) {
        this.localizer = localizer;
    }

    onChanged(ev: any) {
        let val = ev.target && ev.target.value;
        this.value = val;
        this.valueChange.emit(this.value);

        let error = this.validator.hasError(ev.target);
        if (error) {
            if (this.localizer) {
                this.validator.showError(ev.target, this.localizer.translate(error));
                return error;
            }

            this.validator.showError(ev.target, error)
            return error;
        }
        
        this.validator.removeError(ev.target);
    }

    render() {
        return (
          <div class={this.theme}>
            <label><slot name="label"></slot></label>
            <select name={this.name} value={this.value} onChange={this.onChanged.bind(this)}>
                <slot></slot>
            </select>
          </div>
        )
    }
    componentWillLoad () {
        this.injector.create().then(validationService => {
            this.validator = validationService;
        });
    }
    componentDidLoad () {
        const selectEl = this.el.querySelector('select');
        if (this.required) {
            selectEl.setAttribute('required', '');
        }
    }
}