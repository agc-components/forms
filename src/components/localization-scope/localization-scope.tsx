import { Component, Prop, Method, Element, Watch } from '@stencil/core';
import { ILocalizationServiceInjector, ILocalizationService } from '../../localization';
import { validationMessages} from '../../validation';

@Component({
    tag: 'localization-scope'
})
export class LocalizationScope {
    @Element() el:HTMLElement;
    @Prop({connect: 'localization-service-injector'}) injector: ILocalizationServiceInjector;
    @Prop() resource?: string;
    @Prop() definition?: any;
    
    localizer:ILocalizationService;

    @Method()
    load(messages:any): void {
        this.localizer.load(messages);
        this.visit();
    }

    @Method()
    translate(str:string): string {
        return this.localizer.translate(str);
    }

    @Method()
    translateWithProps(str:string, props:any): string {
        return this.localizer.translateWithProps(str, props);
    }

    @Watch('resource')
    resourceChanged () {
        this.inject();
    }

    @Watch('definition')
    definitionChanged () {
        this.inject();
    }

    render() {
        return (
          <div>
            <slot name="definition"></slot>
            <slot></slot>
          </div>
        )
    }
    visit() {
        let subjects = this.el.querySelectorAll('locale-text, form-input, form-textarea, form-select');
            Array.prototype.forEach.call(subjects, s => {
                s.visit && s.visit(this.localizer);
            });
    }
    inject () {
        let injector = this.injector;
        let scriptTag = this.el.querySelector('script[slot="definition"]');
        if (scriptTag && scriptTag.getAttribute('type') === 'application/json') {            
            
            let json = Object.assign({}, JSON.parse(scriptTag.innerHTML));
            let def = {...validationMessages, ...json};
            this.definition = def;
            injector.create().then(localizationService => {
                this.localizer = localizationService;
                this.localizer.load(def);
                this.visit();
            });
        } else if (this.definition != undefined) {
            let def = {...validationMessages, ...this.definition };
            injector.create().then(localizationService => {
                this.localizer = localizationService;
                this.localizer.load(def);
                this.visit();
            });
        } else if (this.resource != undefined) {
            injector.create(this.resource).then(localizationService => {
                this.localizer = localizationService;                
                this.visit();                
            });
        } else {
            injector.create().then(localizationService => {
                this.localizer = localizationService;
                this.localizer.load(validationMessages);
                this.visit();
            });
        }
    }
    componentDidLoad () {
        this.inject.call(this);
    }
}