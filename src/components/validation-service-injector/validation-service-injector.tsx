import {Component, Method} from '@stencil/core';
import {ValidationService, IValidationServiceInjector, IValidationService} from "../../validation";

let validationService = new ValidationService();

@Component({
    tag: 'validation-service-injector'
})
export class ValidationServiceInjector implements IValidationServiceInjector {
    @Method()
    create(): Promise<IValidationService> {
        
        return new Promise(resolve => {
            resolve(validationService);
        });
    }
}