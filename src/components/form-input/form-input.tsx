import { Component, Element, Event, EventEmitter, Prop, Method, Watch } from '@stencil/core';
import { IValidationServiceInjector, IValidationService } from '../../validation';
import { ILocalizationService } from '../../localization';

@Component({
    tag: 'form-input',
    styleUrl: 'form-input.css'
})
export class FormInput {
    @Element() el:HTMLElement;
    @Prop({connect: 'validation-service-injector'}) injector: IValidationServiceInjector;
    @Prop() type?: string = 'text';
    @Prop() name: string;
    @Prop() theme?: string = 'form-input';
    @Prop({mutable: true}) value: string;
    @Prop() required?: string;
    @Prop() pattern?: string;
    @Prop() checked?: string;
    @Event() valueChange: EventEmitter;
    validator:IValidationService;
    localizer:ILocalizationService;

    @Watch('value')
    valueChanged() {
        const inputEl = this.el.querySelector('input');
        if (inputEl.value !== this.value) {
            inputEl.value = this.value;
        }
    }

    @Method()
    visit(localizer:ILocalizationService) {
        this.localizer = localizer;
    }

    inputChanged(ev: any) {
        let val = ev.target && ev.target.value;
        this.value = val;
        this.valueChange.emit(this.value);

        let error = this.validator.hasError(ev.target);
        if (error) {
            if (this.localizer) {
                this.validator.showError(ev.target, this.localizer.translate(error));
                return error;
            }

            this.validator.showError(ev.target, error)
            return error;
        }
        
        this.validator.removeError(ev.target);
    }

    render() {
        return (
          <div class={this.theme} data-input-type={this.type}>
            <label><slot name="label"></slot></label>
            <input name={this.name} type={this.type} value={this.value} onInput={this.inputChanged.bind(this)}></input>
            <slot name="label-right"></slot>
          </div>
        )
    }
    componentWillLoad () {
        this.injector.create().then(validationService => {
            this.validator = validationService;
        });
    }
    componentDidLoad () {
        const inputEl = this.el.querySelector('input');
        if (this.required) {
            inputEl.setAttribute('required', '');
        }
        if (this.pattern) {
            inputEl.setAttribute('pattern', this.pattern);
        }
        if (this.checked && this.checked.toLowerCase() === "true") {
            inputEl.setAttribute('checked', 'checked');
        }
    }
}