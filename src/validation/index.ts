export const VALUE_MISSING = 'value_missing';
export const TYPE_MISMATCH = 'type_mismatch';
export const TOO_SHORT = 'too_short';
export const TOO_LONG = 'too_long';
export const BAD_INPUT = 'bad_input';
export const STEP_MISMATCH = 'step_mismatch';
export const RANGE_OVERFLOW = 'range_overflow';
export const RANGE_UNDERFLOW = 'range_underflow';
export const PATTERN_MISMATCH = 'pattern_mismatch';
export const INVALID_EMAIL = 'invalid_email';
export const INVALID_URL = 'invalid_url';
export const INVALID = 'invalid';

export const validationMessages = {
    "value_missing": "Please fill out this field.",
    "type_mismatch": "Please use the correct input type.",
    "invalid_email": "Please enter an email address",
    "invalid_url": "Please enter a URL.",
    "too_short": "Please lengthen this text.",
    "too_long": "Please shorten this text.",
    "bad_input": "Please enter a number.",
    "step_mismatch": "Please select a valid value.",
    "range_overflow": "Please select a smaller value.",
    "range_underflow": "Please select a larger value.",
    "pattern_mismatch": "Please match the requested format.",
    "invalid": "This field is invalid.",
    "characters_remaining": "characters remaining"
}

export interface IValidationServiceInjector {
    create(): Promise<IValidationService>
}

export interface IValidationService {
    hasError(field:HTMLInputElement): string;
    showError (field: HTMLInputElement, error: string): void;
    removeError (field: HTMLInputElement): void;
}

export class ValidationService implements IValidationService {
    hasError(field: HTMLInputElement): string {
        let bypassTypes = ",file,reset,submit,button,";
        if (field.disabled || bypassTypes.indexOf(field.type) != -1) {
            return;
        }
    
        let validity = field.validity;
    
        // If valid, return null
        if (validity.valid) return;
    
        // If field is required and empty
        if (validity.valueMissing) return VALUE_MISSING;
    
        // If not the right type
        if (validity.typeMismatch) {
            // Email
            if (field.type === 'email') return INVALID_EMAIL;
    
            // URL
            if (field.type === 'url') return INVALID_URL;
    
            return TYPE_MISMATCH;
        }
    
        // If too short
        if (validity.tooShort) return TOO_SHORT;
    
        // If too long
        if (validity.tooLong) return TOO_LONG;
    
        // If number input isn't a number
        if (validity.badInput) return BAD_INPUT;
    
        // If a number value doesn't match the step interval
        if (validity.stepMismatch) return STEP_MISMATCH;
    
        // If a number field is over the max
        if (validity.rangeOverflow) return RANGE_OVERFLOW;
    
        // If a number field is below the min
        if (validity.rangeUnderflow) return RANGE_UNDERFLOW;
    
        // If pattern doesn't match
        if (validity.patternMismatch) {
            // If pattern info is included, return custom error
            if (field.hasAttribute('title')) return field.getAttribute('title');
    
            return PATTERN_MISMATCH;
        }
    
        // If all else fails, return a generic catchall error
        return INVALID;
    }
    showError (field: HTMLInputElement, error: string) {
        // Add error class to field
        field.classList.add('error');
    
        // If the field is a radio button and part of a group, error all and get the last item in the group
        if (field.type === 'radio' && field.name) {
            let group = <NodeListOf<HTMLInputElement>>document.getElementsByName(field.name);
            if (group.length > 0) {
                for (var i = 0; i < group.length; i++) {
                    // Only check fields in current form
                    if (group[i].form !== field.form) continue;
                    group[i].classList.add('error');
                }
                field = group[group.length - 1];
            }
        }
    
        // Get field id or name
        let id = field.id || field.name;
        if (!id) return;

        // no form??
        if (!field.form) return;
    
        // Check if error message field already exists
        // If not, create one
        let message = <HTMLElement>field.form.querySelector('.error-message#error-for-' + id );
        if (!message) {
            message = document.createElement('div');
            message.className = 'error-message';
            message.id = 'error-for-' + id;
    
            // If the field is a radio button or checkbox, insert error after the label
            let label;
            if (field.type === 'radio' || field.type ==='checkbox') {
                label = field.form.querySelector('label[for="' + id + '"]') || field.parentNode;
                if (label) {
                    label.parentNode.insertBefore( message, label.nextSibling );
                }
            }
            
            field.parentNode.insertBefore( message, field.nextSibling );
        }
    
        // Add ARIA role to the field
        field.setAttribute('aria-describedby', 'error-for-' + id);
    
        // Update error message
        message.innerHTML = error;
    
        // Show error message
        message.style.display = 'block';
        message.style.visibility = 'visible';
    }
    removeError (field: HTMLInputElement) {
        // Remove error class to field
        field.classList.remove('error');
    
        // If the field is a radio button and part of a group, remove error from all and get the last item in the group
        if (field.type === 'radio' && field.name) {
            let group = <NodeListOf<HTMLInputElement>>document.getElementsByName(field.name);
            if (group.length > 0) {
                for (var i = 0; i < group.length; i++) {
                    // Only check fields in current form
                    if (group[i].form !== field.form) continue;
                    group[i].classList.remove('error');
                }
                field = group[group.length - 1];
            }
        }
    
        // Remove ARIA role from the field
        field.removeAttribute('aria-describedby');
    
        // Get field id or name
        var id = field.id || field.name;
        if (!id) return;
    
        // Check if an error message is in the DOM
        var message = <HTMLElement>field.form.querySelector('.error-message#error-for-' + id + '');
        if (!message) return;
    
        // If so, hide it
        message.innerHTML = '';
        message.style.display = 'none';
        message.style.visibility = 'hidden';
    }
}

