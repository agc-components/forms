/**
 * This is an autogenerated file created by the Stencil build process.
 * It contains typing information for all components that exist in this project
 * and imports for stencil collections that might be configured in your stencil.config.js file
 */


declare global {
  interface HTMLStencilElement extends HTMLElement {
    componentOnReady(): Promise<this>;
    componentOnReady(done: (ele?: this) => void): void;
  }
}



import {
  FormInputGroup as FormInputGroup
} from './components/form-input-group/form-input-group';

declare global {
  interface HTMLFormInputGroupElement extends FormInputGroup, HTMLStencilElement {
  }
  var HTMLFormInputGroupElement: {
    prototype: HTMLFormInputGroupElement;
    new (): HTMLFormInputGroupElement;
  };
  interface HTMLElementTagNameMap {
    "form-input-group": HTMLFormInputGroupElement;
  }
  interface ElementTagNameMap {
    "form-input-group": HTMLFormInputGroupElement;
  }
  namespace JSX {
    interface IntrinsicElements {
      "form-input-group": JSXElements.FormInputGroupAttributes;
    }
  }
  namespace JSXElements {
    export interface FormInputGroupAttributes extends HTMLAttributes {
      flexible?: string;
      theme?: string;
    }
  }
}


import {
  FormInput as FormInput
} from './components/form-input/form-input';

declare global {
  interface HTMLFormInputElement extends FormInput, HTMLStencilElement {
  }
  var HTMLFormInputElement: {
    prototype: HTMLFormInputElement;
    new (): HTMLFormInputElement;
  };
  interface HTMLElementTagNameMap {
    "form-input": HTMLFormInputElement;
  }
  interface ElementTagNameMap {
    "form-input": HTMLFormInputElement;
  }
  namespace JSX {
    interface IntrinsicElements {
      "form-input": JSXElements.FormInputAttributes;
    }
  }
  namespace JSXElements {
    export interface FormInputAttributes extends HTMLAttributes {
      checked?: string;
      name?: string;
      pattern?: string;
      required?: string;
      theme?: string;
      type?: string;
      value?: string;
    }
  }
}


import {
  FormSelect as FormSelect
} from './components/form-select/form-select';

declare global {
  interface HTMLFormSelectElement extends FormSelect, HTMLStencilElement {
  }
  var HTMLFormSelectElement: {
    prototype: HTMLFormSelectElement;
    new (): HTMLFormSelectElement;
  };
  interface HTMLElementTagNameMap {
    "form-select": HTMLFormSelectElement;
  }
  interface ElementTagNameMap {
    "form-select": HTMLFormSelectElement;
  }
  namespace JSX {
    interface IntrinsicElements {
      "form-select": JSXElements.FormSelectAttributes;
    }
  }
  namespace JSXElements {
    export interface FormSelectAttributes extends HTMLAttributes {
      name?: string;
      required?: string;
      theme?: string;
      value?: string;
    }
  }
}


import {
  FormTextarea as FormTextarea
} from './components/form-textarea/form-textarea';

declare global {
  interface HTMLFormTextareaElement extends FormTextarea, HTMLStencilElement {
  }
  var HTMLFormTextareaElement: {
    prototype: HTMLFormTextareaElement;
    new (): HTMLFormTextareaElement;
  };
  interface HTMLElementTagNameMap {
    "form-textarea": HTMLFormTextareaElement;
  }
  interface ElementTagNameMap {
    "form-textarea": HTMLFormTextareaElement;
  }
  namespace JSX {
    interface IntrinsicElements {
      "form-textarea": JSXElements.FormTextareaAttributes;
    }
  }
  namespace JSXElements {
    export interface FormTextareaAttributes extends HTMLAttributes {
      lines?: number;
      maxLength?: number;
      name?: string;
      pattern?: string;
      required?: string;
      theme?: string;
      value?: string;
    }
  }
}


import {
  LocaleText as LocaleText
} from './components/locale-text/locale-text';

declare global {
  interface HTMLLocaleTextElement extends LocaleText, HTMLStencilElement {
  }
  var HTMLLocaleTextElement: {
    prototype: HTMLLocaleTextElement;
    new (): HTMLLocaleTextElement;
  };
  interface HTMLElementTagNameMap {
    "locale-text": HTMLLocaleTextElement;
  }
  interface ElementTagNameMap {
    "locale-text": HTMLLocaleTextElement;
  }
  namespace JSX {
    interface IntrinsicElements {
      "locale-text": JSXElements.LocaleTextAttributes;
    }
  }
  namespace JSXElements {
    export interface LocaleTextAttributes extends HTMLAttributes {
      context?: any;
      id?: string;
      theme?: string;
    }
  }
}


import {
  LocalizationScope as LocalizationScope
} from './components/localization-scope/localization-scope';

declare global {
  interface HTMLLocalizationScopeElement extends LocalizationScope, HTMLStencilElement {
  }
  var HTMLLocalizationScopeElement: {
    prototype: HTMLLocalizationScopeElement;
    new (): HTMLLocalizationScopeElement;
  };
  interface HTMLElementTagNameMap {
    "localization-scope": HTMLLocalizationScopeElement;
  }
  interface ElementTagNameMap {
    "localization-scope": HTMLLocalizationScopeElement;
  }
  namespace JSX {
    interface IntrinsicElements {
      "localization-scope": JSXElements.LocalizationScopeAttributes;
    }
  }
  namespace JSXElements {
    export interface LocalizationScopeAttributes extends HTMLAttributes {
      definition?: any;
      resource?: string;
    }
  }
}


import {
  LocalizationServiceInjector as LocalizationServiceInjector
} from './components/localization-service-injector/localization-service-injector';

declare global {
  interface HTMLLocalizationServiceInjectorElement extends LocalizationServiceInjector, HTMLStencilElement {
  }
  var HTMLLocalizationServiceInjectorElement: {
    prototype: HTMLLocalizationServiceInjectorElement;
    new (): HTMLLocalizationServiceInjectorElement;
  };
  interface HTMLElementTagNameMap {
    "localization-service-injector": HTMLLocalizationServiceInjectorElement;
  }
  interface ElementTagNameMap {
    "localization-service-injector": HTMLLocalizationServiceInjectorElement;
  }
  namespace JSX {
    interface IntrinsicElements {
      "localization-service-injector": JSXElements.LocalizationServiceInjectorAttributes;
    }
  }
  namespace JSXElements {
    export interface LocalizationServiceInjectorAttributes extends HTMLAttributes {
      
    }
  }
}


import {
  ValidationServiceInjector as ValidationServiceInjector
} from './components/validation-service-injector/validation-service-injector';

declare global {
  interface HTMLValidationServiceInjectorElement extends ValidationServiceInjector, HTMLStencilElement {
  }
  var HTMLValidationServiceInjectorElement: {
    prototype: HTMLValidationServiceInjectorElement;
    new (): HTMLValidationServiceInjectorElement;
  };
  interface HTMLElementTagNameMap {
    "validation-service-injector": HTMLValidationServiceInjectorElement;
  }
  interface ElementTagNameMap {
    "validation-service-injector": HTMLValidationServiceInjectorElement;
  }
  namespace JSX {
    interface IntrinsicElements {
      "validation-service-injector": JSXElements.ValidationServiceInjectorAttributes;
    }
  }
  namespace JSXElements {
    export interface ValidationServiceInjectorAttributes extends HTMLAttributes {
      
    }
  }
}

declare global { namespace JSX { interface StencilJSX {} } }
