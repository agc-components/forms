import { template } from '../util';

export interface ILocalizationServiceInjector {
    create(localeUrl?:string): Promise<ILocalizationService>;    
}

export interface ILocalizationService {
    load(messages:any): void;
    translate(str:string): string;
    translateWithProps(str:string, props:any): string;
}

export class LocalizationService implements ILocalizationService {

    messages:any;

    constructor(messages:any) {
        this.messages = messages || {};
    }

    load(messages:any) : void {
        this.messages = messages;
    }

    translate(str:string): string {
        if (this.messages && this.messages.hasOwnProperty(str)) {
            return this.messages[str];
        }
        return null;
    }

    translateWithProps(str:string, props:any): string {
        
        if (this.messages && this.messages.hasOwnProperty(str)) {
            return template(this.messages[str], props);
        }
        
        return null;
    }
}