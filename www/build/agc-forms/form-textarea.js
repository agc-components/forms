/*! Built with http://stenciljs.com */
const { h, Context } = window.AgcForms;

class FormTextarea {
    constructor() {
        this.theme = 'form-input';
        this.lines = 5;
    }
    visit(localizer) {
        this.localizer = localizer;
    }
    inputChanged(ev) {
        let val = ev.target && ev.target.value;
        if (this.maxLength != undefined) {
            let charactersLeft = this.maxLength - val.length;
            this.remaining = charactersLeft > 0 ? charactersLeft : 0;
            if (this.remaining <= 0) {
                val = val.slice(0, this.maxLength);
                const textarea = this.el.querySelector('textarea');
                textarea.value = val;
            }
        }
        this.value = val;
        this.valueChange.emit(this.value);
        let error = this.validator.hasError(ev.target);
        if (error) {
            if (this.localizer) {
                this.errorMessage = this.localizer.translate(error);
                return error;
            }
            this.errorMessage = error;
            return error;
        }
        this.errorMessage = null;
    }
    render() {
        return (h("div", { class: this.theme, "data-input-type": "textarea" },
            h("label", null,
                h("slot", { name: "label" })),
            h("textarea", { rows: this.lines, id: this.name, name: this.name, onInput: this.inputChanged.bind(this) }, this.value),
            h("p", null,
                this.errorMessage && h("span", { class: "error-message" }, this.errorMessage),
                this.maxLength && h("span", null,
                    this.remaining,
                    " ",
                    h("locale-text", { id: "characters_remaining" }, "characters remaining")))));
    }
    componentWillLoad() {
        this.injector.create().then(validationService => {
            this.validator = validationService;
        });
    }
    componentDidLoad() {
        if (this.maxLength) {
            this.remaining = this.maxLength;
        }
        const inputEl = this.el.querySelector('textarea');
        if (this.required) {
            inputEl.setAttribute('required', '');
        }
        if (this.pattern) {
            inputEl.setAttribute('pattern', this.pattern);
        }
    }
    static get is() { return "form-textarea"; }
    static get properties() { return { "el": { "elementRef": true }, "errorMessage": { "state": true }, "injector": { "connect": "validation-service-injector" }, "lines": { "type": Number, "attr": "lines" }, "maxLength": { "type": Number, "attr": "max-length" }, "name": { "type": String, "attr": "name" }, "pattern": { "type": String, "attr": "pattern" }, "remaining": { "state": true }, "required": { "type": String, "attr": "required" }, "theme": { "type": String, "attr": "theme" }, "value": { "type": String, "attr": "value", "mutable": true }, "visit": { "method": true } }; }
    static get events() { return [{ "name": "valueChange", "method": "valueChange", "bubbles": true, "cancelable": true, "composed": true }]; }
    static get style() { return "/**\n * Form Styles\n */\n .form-input label {\n    display: block;\n    font-weight: bold;\n    margin-bottom: 0.5em;\n}\n\n.form-input .label-normal {\n    font-weight: normal;\n}\n\n.form-input textarea {\n    -moz-box-sizing: border-box;\n    box-sizing: border-box;\n    font-size: 1em;\n    \n    padding: 0.25em 0.5em;\n    width: 100%;\n}\n.form-input textarea + p {\n    margin: 0;\n    text-align: right;\n    margin-bottom: 1em;\n    color: #777777;\n}\n/**\n * Errors\n */\nform-textarea .error {\n    border-color: red;\n}\n\nform-textarea .error-message {\n    float: left;\n    color: red;\n    font-style: italic;\n    margin-bottom: 1em;\n}"; }
}

class LocaleText {
    constructor() {
        this.theme = "locale-text";
    }
    render() {
        return (h("span", { class: this.theme },
            h("slot", null)));
    }
    visit(translator) {
        let ctx = null;
        if (this.context && typeof this.context === 'string') {
            ctx = JSON.parse(this.context.replace(/'/g, '"'));
        }
        else {
            ctx = this.context;
        }
        let translation = ctx && translator.translateWithProps(this.id, ctx)
            || translator.translate(this.id);
        if (!translation) {
            return;
        }
        this.el.innerText = translation;
    }
    componentWillLoad() {
    }
    static get is() { return "locale-text"; }
    static get properties() { return { "context": { "type": "Any", "attr": "context" }, "el": { "elementRef": true }, "id": { "type": String, "attr": "id" }, "theme": { "type": String, "attr": "theme" }, "visit": { "method": true } }; }
}

export { FormTextarea, LocaleText };
