/*! Built with http://stenciljs.com */
const { h, Context } = window.AgcForms;

import { ValidationService } from './chunk1.js';

let validationService = new ValidationService();
class ValidationServiceInjector {
    create() {
        return new Promise(resolve => {
            resolve(validationService);
        });
    }
    static get is() { return "validation-service-injector"; }
    static get properties() { return { "create": { "method": true } }; }
}

export { ValidationServiceInjector };
