/*! Built with http://stenciljs.com */
const { h, Context } = window.AgcForms;

class FormInput {
    constructor() {
        this.type = 'text';
        this.theme = 'form-input';
    }
    valueChanged() {
        const inputEl = this.el.querySelector('input');
        if (inputEl.value !== this.value) {
            inputEl.value = this.value;
        }
    }
    visit(localizer) {
        this.localizer = localizer;
    }
    inputChanged(ev) {
        let val = ev.target && ev.target.value;
        this.value = val;
        this.valueChange.emit(this.value);
        let error = this.validator.hasError(ev.target);
        if (error) {
            if (this.localizer) {
                this.validator.showError(ev.target, this.localizer.translate(error));
                return error;
            }
            this.validator.showError(ev.target, error);
            return error;
        }
        this.validator.removeError(ev.target);
    }
    render() {
        return (h("div", { class: this.theme, "data-input-type": this.type },
            h("label", null,
                h("slot", { name: "label" })),
            h("input", { name: this.name, type: this.type, value: this.value, onInput: this.inputChanged.bind(this) }),
            h("slot", { name: "label-right" })));
    }
    componentWillLoad() {
        this.injector.create().then(validationService => {
            this.validator = validationService;
        });
    }
    componentDidLoad() {
        const inputEl = this.el.querySelector('input');
        if (this.required) {
            inputEl.setAttribute('required', '');
        }
        if (this.pattern) {
            inputEl.setAttribute('pattern', this.pattern);
        }
        if (this.checked && this.checked.toLowerCase() === "true") {
            inputEl.setAttribute('checked', 'checked');
        }
    }
    static get is() { return "form-input"; }
    static get properties() { return { "checked": { "type": String, "attr": "checked" }, "el": { "elementRef": true }, "injector": { "connect": "validation-service-injector" }, "name": { "type": String, "attr": "name" }, "pattern": { "type": String, "attr": "pattern" }, "required": { "type": String, "attr": "required" }, "theme": { "type": String, "attr": "theme" }, "type": { "type": String, "attr": "type" }, "value": { "type": String, "attr": "value", "mutable": true, "watchCallbacks": ["valueChanged"] }, "visit": { "method": true } }; }
    static get events() { return [{ "name": "valueChange", "method": "valueChange", "bubbles": true, "cancelable": true, "composed": true }]; }
    static get style() { return "/**\n * Form Styles\n */\n.form-input label {\n    display: block;\n    font-weight: bold;\n    margin-bottom: 0.5em;\n}\n\n.form-input .label-normal {\n    font-weight: normal;\n}\n\n.form-input[data-input-type=\"radio\"] label,\n.form-input[data-input-type=\"checkbox\"] label {\n    display: inline;\n}\n\n.form-input input[type=\"text\"], \n.form-input input[type=\"password\"],\n.form-input input[type=\"date\"] {\n    -moz-box-sizing: border-box;\n    box-sizing: border-box;\n    font-size: 1em;\n    margin-bottom: 1em;\n    padding: 0.25em 0.5em;\n    width: 100%;\n}\n/**\n * Errors\n */\nform-input .error {\n    border-color: red;\n}\n\nform-input .error-message {\n    color: red;\n    font-style: italic;\n    margin-bottom: 1em;\n}"; }
}

export { FormInput };
