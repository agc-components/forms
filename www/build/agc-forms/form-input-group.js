/*! Built with http://stenciljs.com */
const { h, Context } = window.AgcForms;

class FormInputGroup {
    constructor() {
        this.theme = 'form-input-group-wrapper';
        this.flexible = "true";
    }
    render() {
        let cls = this.theme;
        if (!!this.flexible) {
            cls = cls + ' flexible';
        }
        return (h("div", { class: cls },
            h("label", null,
                h("slot", { name: "label" })),
            h("div", { class: 'form-input-group' },
                h("slot", null))));
    }
    componentDidLoad() {
        const children = this.el.querySelectorAll('form-input, form-select');
        console.log('children', children);
    }
    static get is() { return "form-input-group"; }
    static get properties() { return { "el": { "elementRef": true }, "flexible": { "type": String, "attr": "flexible" }, "theme": { "type": String, "attr": "theme" } }; }
    static get style() { return "/**\n * Form Styles\n */\n\n .form-input-group-wrapper label {\n    display: block;\n    font-weight: bold;\n    margin-bottom: 0.5em;\n}\n.form-input-group-wrapper > label {\n    padding-bottom: 0.5em;\n    border-bottom: 2px solid #dcdcdc;\n}\n\n.form-input-group {\n    margin-bottom: 1em;\n}\n\n.form-input-group .label-normal {\n    font-weight: normal;\n}\n\n.form-input-group {\n    display: flex;\n    margin: 0 -15px;\n    align-items: center;\n    overflow: hidden;\n    padding: 1em 0;\n}\n.form-input-group form-input, .form-input-group form-select {\n    display: inline-block;\n    padding:0 15px;\n    margin-right: 1em;\n}\n.form-input-group form-input:first, .form-input-group form-select:first {\n    color: red;\n}\n.form-input-group-wrapper.flexible form-input, .form-input-group-wrapper.flexible form-select {\n    margin: 0;\n    flex: 1 1 auto;\n    margin: 5px;\n}"; }
}

export { FormInputGroup };
