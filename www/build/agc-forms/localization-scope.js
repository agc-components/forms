/*! Built with http://stenciljs.com */
const { h, Context } = window.AgcForms;

import { validationMessages } from './chunk1.js';

class LocalizationScope {
    load(messages) {
        this.localizer.load(messages);
        this.visit();
    }
    translate(str) {
        return this.localizer.translate(str);
    }
    translateWithProps(str, props) {
        return this.localizer.translateWithProps(str, props);
    }
    resourceChanged() {
        this.inject();
    }
    definitionChanged() {
        this.inject();
    }
    render() {
        return (h("div", null,
            h("slot", { name: "definition" }),
            h("slot", null)));
    }
    visit() {
        let subjects = this.el.querySelectorAll('locale-text, form-input, form-textarea, form-select');
        Array.prototype.forEach.call(subjects, s => {
            s.visit && s.visit(this.localizer);
        });
    }
    inject() {
        let injector = this.injector;
        let scriptTag = this.el.querySelector('script[slot="definition"]');
        if (scriptTag && scriptTag.getAttribute('type') === 'application/json') {
            let json = Object.assign({}, JSON.parse(scriptTag.innerHTML));
            let def = Object.assign({}, validationMessages, json);
            this.definition = def;
            injector.create().then(localizationService => {
                this.localizer = localizationService;
                this.localizer.load(def);
                this.visit();
            });
        }
        else if (this.definition != undefined) {
            let def = Object.assign({}, validationMessages, this.definition);
            injector.create().then(localizationService => {
                this.localizer = localizationService;
                this.localizer.load(def);
                this.visit();
            });
        }
        else if (this.resource != undefined) {
            injector.create(this.resource).then(localizationService => {
                this.localizer = localizationService;
                this.visit();
            });
        }
        else {
            injector.create().then(localizationService => {
                this.localizer = localizationService;
                this.localizer.load(validationMessages);
                this.visit();
            });
        }
    }
    componentDidLoad() {
        this.inject.call(this);
    }
    static get is() { return "localization-scope"; }
    static get properties() { return { "definition": { "type": "Any", "attr": "definition", "watchCallbacks": ["definitionChanged"] }, "el": { "elementRef": true }, "injector": { "connect": "localization-service-injector" }, "load": { "method": true }, "resource": { "type": String, "attr": "resource", "watchCallbacks": ["resourceChanged"] }, "translate": { "method": true }, "translateWithProps": { "method": true } }; }
}

function jsonGet(url, callback) {
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            try {
                var data = JSON.parse(xmlhttp.responseText);
            }
            catch (err) {
                console.log(err.message + " in " + xmlhttp.responseText);
                return;
            }
            callback(data);
        }
    };
    xmlhttp.open("GET", url, true);
    xmlhttp.send();
}


function template(html, options) {
    var re = /\{\{(.+?)\}\}/g, reExp = /(^( )?(var|if|for|else|switch|case|break|{|}|;))(.*)?/g, code = 'with(obj) { var r=[];\n', cursor = 0, result, match;
    var add = function (line, js) {
        js ? (code += line.match(reExp) ? line + '\n' : 'r.push(' + line + ');\n') :
            (code += line != '' ? 'r.push("' + line.replace(/"/g, '\\"') + '");\n' : '');
        return add;
    };
    while (match = re.exec(html)) {
        add(html.slice(cursor, match.index), null)(match[1], true);
        cursor = match.index + match[0].length;
    }
    add(html.substr(cursor, html.length - cursor), null);
    code = (code + 'return r.join(""); }').replace(/[\r\t\n]/g, ' ');
    try {
        result = new Function('obj', code).apply(options, [options]);
    }
    catch (err) {
        console.error("'" + err.message + "'", " in \n\nCode:\n", code, "\n");
    }
    return result;
}

class LocalizationService {
    constructor(messages) {
        this.messages = messages || {};
    }
    load(messages) {
        this.messages = messages;
    }
    translate(str) {
        if (this.messages && this.messages.hasOwnProperty(str)) {
            return this.messages[str];
        }
        return null;
    }
    translateWithProps(str, props) {
        if (this.messages && this.messages.hasOwnProperty(str)) {
            return template(this.messages[str], props);
        }
        return null;
    }
}

class LocalizationServiceInjector {
    create(localeUrl) {
        return new Promise(resolve => {
            if (localeUrl != undefined) {
                jsonGet(localeUrl, (messages) => {
                    resolve(new LocalizationService(messages));
                });
            }
            else {
                resolve(new LocalizationService({}));
            }
        });
    }
    static get is() { return "localization-service-injector"; }
    static get properties() { return { "create": { "method": true } }; }
}

export { LocalizationScope, LocalizationServiceInjector };
