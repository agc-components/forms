/*! Built with http://stenciljs.com */
const { h, Context } = window.AgcForms;

class MediaQueryService {
    constructor(xs, sm, md, lg) {
        this.xs = xs;
        this.sm = sm;
        this.md = md;
        this.lg = lg;
    }
}

class MediaQueryServiceInjector {
    create(xs, sm, md, lg) {
        return new Promise(resolve => {
            resolve(new MediaQueryService(xs, sm, md, lg));
        });
    }
    breakpointChangedHandler(breakpoint) {
        this.breakpointChanged.emit(breakpoint);
    }
    static get is() { return "mediaquery-service-injector"; }
    static get properties() { return { "create": { "method": true } }; }
    static get events() { return [{ "name": "breakpointChanged", "method": "breakpointChanged", "bubbles": true, "cancelable": true, "composed": true }]; }
}

export { MediaQueryServiceInjector as MediaqueryServiceInjector };
