/*! Built with http://stenciljs.com */
const { h, Context } = window.AgcForms;

class FormSelect {
    constructor() {
        this.theme = 'form-input';
    }
    valueChanged() {
        const selectEl = this.el.querySelector('select');
        if (selectEl.value !== this.value) {
            selectEl.value = this.value;
        }
    }
    visit(localizer) {
        this.localizer = localizer;
    }
    onChanged(ev) {
        let val = ev.target && ev.target.value;
        this.value = val;
        this.valueChange.emit(this.value);
        let error = this.validator.hasError(ev.target);
        if (error) {
            if (this.localizer) {
                this.validator.showError(ev.target, this.localizer.translate(error));
                return error;
            }
            this.validator.showError(ev.target, error);
            return error;
        }
        this.validator.removeError(ev.target);
    }
    render() {
        return (h("div", { class: this.theme },
            h("label", null,
                h("slot", { name: "label" })),
            h("select", { name: this.name, value: this.value, onChange: this.onChanged.bind(this) },
                h("slot", null))));
    }
    componentWillLoad() {
        this.injector.create().then(validationService => {
            this.validator = validationService;
        });
    }
    componentDidLoad() {
        const selectEl = this.el.querySelector('select');
        if (this.required) {
            selectEl.setAttribute('required', '');
        }
    }
    static get is() { return "form-select"; }
    static get properties() { return { "el": { "elementRef": true }, "injector": { "connect": "validation-service-injector" }, "name": { "type": String, "attr": "name" }, "required": { "type": String, "attr": "required" }, "theme": { "type": String, "attr": "theme" }, "value": { "type": String, "attr": "value", "mutable": true, "watchCallbacks": ["valueChanged"] }, "visit": { "method": true } }; }
    static get events() { return [{ "name": "valueChange", "method": "valueChange", "bubbles": true, "cancelable": true, "composed": true }]; }
    static get style() { return "/**\n * Form Styles\n */\n .form-input label {\n    display: block;\n    font-weight: bold;\n    margin-bottom: 0.5em;\n}\n\n.form-input .label-normal {\n    font-weight: normal;\n}\n\n.form-input select {\n    -moz-box-sizing: border-box;\n    box-sizing: border-box;\n    font-size: 1em;\n    margin-bottom: 1em;\n    padding: 0.25em 0.5em;\n    width: 100%;\n}\n/**\n * Errors\n */\nform-select .error {\n    border-color: red;\n}\n\nform-select .error-message {\n    color: red;\n    font-style: italic;\n    margin-bottom: 1em;\n}"; }
}

export { FormSelect };
